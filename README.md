快捷扫码下载（Fast-ScanQRCode-Download）就是这么直接的翻译

纯绿色，开源..
依赖java环境、局域网

# [马上下载试用](https://gitee.com/dongnao_tony/fast-scanqrcode-download/attach_files)

### 为什么要写这个
电脑传文件到手机上，百度搜索出来的工具，操作太麻烦，真是烦死了...没找到方便的工具，自己做一个吧。。


### 使用方法
1、命令行模式
```
bin/Fast-ScanQRCode-Download.bat "文件全路径"
```

2、 右键菜单方式（需要添加右键菜单，windows下我写了一个简单的批处理脚本），管理员权限执行install.bat即可

### 待完善的地方
1、windows下运行，会弹出控制台那个黑窗...感觉很不爽

2、linux下还没试呢..

### 来个动图演示一下功能...
1、windows下右键菜单生成二维码

![输入图片说明](https://thumbnail0.baidupcs.com/thumbnail/e6cca364ccea93ff605313863f5283b1?fid=2754499570-250528-1101733327571362&time=1497157200&rt=sh&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-2BywWE15YGM%2FkuaG2d8dsOxRNwA%3D&expires=8h&chkv=0&chkbd=0&chkpc=&dp-logid=3747471270611076119&dp-callid=0&size=c710_u400&quality=100&vuk=-&ft=video "在这里输入图片标题")

2、手机端扫码下载

![输入图片说明](https://thumbnail0.baidupcs.com/thumbnail/91145fe661551f2dda1cea42b55add76?fid=2754499570-250528-829269160793557&time=1497157200&rt=sh&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-QvMs38aN6IBK%2FA8%2BFmdCYFUT3NY%3D&expires=8h&chkv=0&chkbd=0&chkpc=&dp-logid=3747490636932139619&dp-callid=0&size=c710_u400&quality=100&vuk=-&ft=video "在这里输入图片标题")


觉得可以就Star，欢迎优化和扩展实用

